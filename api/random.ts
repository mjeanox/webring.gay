import {
	VercelRequest as Request,
	VercelResponse as Response,
} from "@vercel/node";
import { readSites } from "../lib/data";

export default async function randomSite(_req: Request, res: Response) {
	const sites = await readSites();
	if (!sites) throw new Error("webring is misconfigued");
	const targetSite = sites[Math.floor(Math.random() * sites.length)];
	res.redirect(307, targetSite);
}
