import {
	VercelRequest as Request,
	VercelResponse as Response,
} from "@vercel/node";
import { readSites } from "../lib/data";
import { findCurrentSite } from "../lib/findCurrentSite";

export default async function prevSite(req: Request, res: Response) {
	const sites = await readSites();
	if (!sites) throw new Error("webring is misconfigued");

	const currentIndex: number | null = await findCurrentSite(req, sites);

	if (currentIndex) {
		const targetSite = sites[(currentIndex + sites.length - 1) % sites.length];
		res.redirect(307, targetSite);
	} else {
		const targetSite = sites[Math.floor(Math.random() * sites.length)];
		res.redirect(307, targetSite);
	}
}
