import type {
	VercelRequest as Request,
} from "@vercel/node";

export async function findCurrentSite(req: Request, sites: string[]) {
	const currentSite = String(req.query.site) || req.headers.referer || "";
	// using sites.findIndex instead of sites.indexOf so that we can (eventually) do some better url normalization
	const currentIndex = sites.findIndex((site) => site === currentSite);

	if (currentIndex === -1) {
		return null;
	} else {
		return currentIndex;
	}
}
